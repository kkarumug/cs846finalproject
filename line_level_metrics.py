import pandas as pd
import json
import networkx as nx
from os.path import join, exists
import os
import re
from collections import defaultdict
from tqdm import  tqdm
import json
from sklearn.metrics import classification_report,f1_score,recall_score,precision_score,confusion_matrix


root_path="/SV/data/"
source_path="/SV/data/all_source_code_xfgs_split"
data_path="/SV/data/XFG"
XFG_paths_json="test_all_xfg_paths.json"
file_paths=defaultdict(list)
with open(join(source_path,XFG_paths_json), "r") as f:
    XFG_paths_all = list(json.load(f))
    # print(XFG_paths_all)

seen_paths=set()
for i in tqdm(XFG_paths_all,total=len(XFG_paths_all)):
    i_path="/".join(i.split("/")[-4:-2])
    # print(join(root_path,"source_code",i_path))
    # print(i_path)
    if i_path not in seen_paths:
        if os.path.isfile(join(root_path,"source_code",i_path)):
            # print("Hi")
            with open(join(root_path,"source_code",i_path),"r") as f:
                    length=len(f.readlines())
                    file_paths[i_path]=[[[[] for i in range(length)],False],[[[] for i in range(length)],False]]
        seen_paths.add(i_path)

csv_path = join(source_path, "test_results.csv")
pred_data=pd.read_csv(csv_path).set_index('file_path').T.to_dict("records")[0]
for i in tqdm(pred_data,total=len(pred_data)):
    # print(i)
    # print("/".join(i.split("/")[-4:-2]))
    xfg = nx.read_gpickle(join(data_path,"/".join(i.split("/")[-4:])))
    for line in xfg.nodes:
            if "/".join(i.split("/")[-4:-2]) in file_paths:
                # print(file_paths["/".join(i.split("\\").split[-4:-2])])
                # print(line,len(file_paths["/".join(i.split("\\")[-4:-2])][0][0]))
                file_paths["/".join(i.split("/")[-4:-2])][0][0][line-1].append(pred_data[i])
                file_paths["/".join(i.split("/")[-4:-2])][0][1]= True
                # print(file_paths["/".join(i.split("\\")[-4:-2])][0][0][line-1])
            else:
                print("Not Ground Truth "+"/".join(i.split("/")[-4:-2]))
# with open('gt_pred.json', 'w') as fp:
#     json.dump(file_paths, fp)

# for i in predictions:
#     predictions[i]=max(predictions[i],key=predictions[i].count)
#
csv_path = join(root_path, "SVCP4CDataset.csv")
vul_data=pd.read_csv(csv_path).set_index('file_path').T.to_dict("records")[0]
count=0
for i in vul_data:
    for k in vul_data[i].split(","):
        if i[2:] in file_paths:
            file_paths[i[2:]][1][0][int(k)-1].append(1)
            count+=1
            file_paths[i[2:]][1][1] = True
# with open('gt_pred1.json', 'w') as fp:
#     json.dump(file_paths, fp)
#
import json

# Serialize data into file:
json.dump(file_paths, open( "/SV/data/all_source_code_xfgs_split/line_level_data.json", 'w' ) )
print("Count ",count)
gt,pred=[],[]
for i in file_paths:
    if file_paths[i][0][1] and file_paths[i][1][1]:
        for j in file_paths[i][0][0]:
            if not j:
                pred.append(0)
            else:
                # print(i,j)
                print(j,max(j, key=j.count))
                pred.append(max(j, key=j.count))
        for j in file_paths[i][1][0]:
            if not j:
                gt.append(0)
            else:
                gt.append(1)
print(len(gt)==len(pred))
print(classification_report(gt,pred))
print(confusion_matrix(gt,pred))
#
#
