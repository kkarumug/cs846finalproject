import networkx as nx
import json,os
from tqdm import  tqdm
from multiprocessing import cpu_count, Manager, Pool
USE_CPU = cpu_count()
import  functools
from os.path import join, exists
os.chdir("\Dataset\SVCP4CDataset\data\ml_dataset")
def process_parallel(i):
    # print(i[78:])
    xfg = nx.read_gpickle(i)
    path=i[78:].split("\\")
    # print(path)
    try:
        if not exists("/".join(path[:3]) + "/"):
            os.makedirs("/".join(path[:3]) + "/")
        nx.write_gpickle(xfg, i[78:])
    except:
        path[1] = path[1][-50:]
        # print(path)
        print("/".join(path[:3]) + "/"+path[-1])
        # print(path)
        if not exists("/".join(path[:3]) + "/"):
            os.makedirs("/".join(path[:3]) + "/")
        nx.write_gpickle(xfg, "/".join(path[:3]) + "/"+path[-1])



def run():
    data_paths=[]
    paths = ['train.json', 'val.json', 'test.json']
    for i in paths:
        path = r"\Dataset\SVCP4CDataset\data\\" + i
        with open(path, "r") as f:
            data_paths.extend(json.load(f))
    # print(data_paths)
    # [i for i in tqdm(map(process_parallel, data_paths), desc=f"xfg paths: ", total=len(data_paths))]

    with Manager() as m:
        pool = Pool(USE_CPU)
        aa = m.list()
        [i for i in tqdm(pool.imap_unordered(process_parallel, data_paths), desc=f"xfg paths: ", total=len(data_paths))]
        pool.close()
        pool.join()


if __name__ == '__main__':
    run()