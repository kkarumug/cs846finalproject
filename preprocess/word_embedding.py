from argparse import ArgumentParser
from typing import cast, List
from omegaconf import OmegaConf, DictConfig
import json
import os
import networkx as nx
from gensim.models import Word2Vec, KeyedVectors
from os import cpu_count
from src.utils import PAD, MASK, UNK
from tqdm import tqdm
from multiprocessing import cpu_count, Manager, Pool
import functools
from os.path import join

SPECIAL_TOKENS = [PAD, UNK, MASK]
USE_CPU = cpu_count()
def configure_arg_parser() -> ArgumentParser:
    arg_parser = ArgumentParser()
    arg_parser.add_argument("-c",
                            "--config",
                            help="Path to YAML configuration file",
                            default="configs/dwk.yaml",
                            type=str)
    return arg_parser

def process_parallel(path: str, split_token: bool):
    """

    Args:
        path:

    Returns:

    """
    xfg = nx.read_gpickle(path)
    tokens_list = list()
    for ln in xfg:
        code_tokens = xfg.nodes[ln]["code_sym_token"]

        if len(code_tokens) != 0:
            tokens_list.append(code_tokens)
    # print(tokens_list)
    return tokens_list


def train_word_embedding(config,source_path: str):
    """
    train word embedding using word2vec

    Args:
        config_path:

    Returns:

    """
    train_json = f"{source_path}/train.json"
    with open(train_json, "r") as f:
        paths = json.load(f)
    # print("/".join(paths[0][74:].split("\\")))
    # print(paths[0].find("ml_dataset"))
    tokens_list = list()
    with Manager():
        pool = Pool(USE_CPU)

        process_func = functools.partial(process_parallel,
                                         split_token=False)
        tokens: List = [
            res
            for res in tqdm(
                pool.imap_unordered(process_func, paths),
                desc=f"xfg paths: ",
                total=len(paths),
            )
        ]
        pool.close()
        pool.join()
    for token_l in tokens:
        tokens_list.extend(token_l)

    print("training w2v...")
    num_workers = 8
    model = Word2Vec(sentences=tokens_list, min_count=3, size=256,
                     max_vocab_size=190000, workers=num_workers, sg=1)
    model.wv.save(join(config.root_folder_path,config.split_folder_name,"w2v.wv"))


def load_wv(config_path: str):
    """

    Args:
        config_path:

    Returns:

    """
    config = cast(DictConfig, OmegaConf.load(config_path))
    cweid = config.dataset.name

    model = KeyedVectors.load(f"{config.data_folder}/{cweid}/w2v.wv", mmap="r")

    print()


if __name__ == '__main__':
    __arg_parser = configure_arg_parser()
    __args = __arg_parser.parse_args()
    config = cast(DictConfig, OmegaConf.load(__args.config))
    train_word_embedding(config,join(config.root_folder_path,config.split_folder_name))
    # load_wv(__args.config)
