from typing import List, cast
from os.path import join
from argparse import ArgumentParser
import os
from src.utils import unique_xfg_sym, split_list
import networkx as nx
from src.preprocess.symbolizer import clean_gadget
from tqdm import tqdm
from omegaconf import OmegaConf, DictConfig
from multiprocessing import cpu_count, Manager, Pool, Queue
import functools
import dataclasses
from src.preprocess.symbolizer import tokenize_code_line
import pickle
USE_CPU = cpu_count()


def configure_arg_parser() -> ArgumentParser:
    arg_parser = ArgumentParser()
    arg_parser.add_argument("-c",
                            "--config",
                            help="Path to YAML configuration file",
                            default="configs/dwk.yaml",
                            type=str)
    return arg_parser


def unique_data(source_path: str,root_folder_path:str, directories: str):
    """
    unique raw data without symbolization
    Args:
        cweid:
        root:

    Returns:

    """
    xfg_paths = list()
    for i in directories:
        if os.path.isdir(join(source_path, i)):
            for testcase in os.listdir(join(source_path, i)):
                testcase_root_path = join(source_path, i, testcase)
                if os.path.isdir(testcase_root_path):
                    folders=os.listdir(testcase_root_path)
                    for k in folders:
                        if os.path.isdir(join(testcase_root_path, k)):
                            k_root_path = join(testcase_root_path, k)
                            xfg_ps = os.listdir(k_root_path)
                            for xfg_p in xfg_ps:
                                xfg_path = join(k_root_path, xfg_p)
                                xfg_paths.append(xfg_path)
    print(xfg_paths)
    # remove duplicates and conflicts
    xfg_dict = unique_xfg_sym(root_folder_path, xfg_paths)
    xfg_unique_paths = list()
    for md5 in xfg_dict:
        # remove conflicts
        if xfg_dict[md5]["label"] != -1:
            xfg_unique_paths.append(xfg_dict[md5]["xfg"])
    return xfg_unique_paths


@dataclasses.dataclass
class QueueMessage:
    XFG: nx.DiGraph
    xfg_path: str
    to_remove: bool = False
    is_finished: bool = False

remove=0
def handle_queue_message(XFG, xfg_path, to_remove):
    global remove
    if to_remove:
        remove+=1
        os.system(f"del {xfg_path}")
    elif XFG is not None:
        nx.write_gpickle(XFG, xfg_path)
    return


# count, count1 = 0, 0


def process_parallel(source_path: str, split_token: bool):
    global count, count1
    """

    Args:
        testcaseid:
        queue:
        XFG_root_path:

    Returns:

    """
    for k in ["arith", "array", "call", "ptr"]:
        k_root_path = join(source_path, k)
        xfg_ps = os.listdir(k_root_path)
        # print(xfg_ps,k_root_path)
        # count1 += len(xfg_ps)
        for xfg_p in xfg_ps:
            xfg_path = join(k_root_path, xfg_p)
            flag=False
            # count += 1
            # ff.write(xfg_path + " " + str(count) + "\n")
            xfg: nx.DiGraph = nx.read_gpickle(xfg_path)
            for idx, nn in enumerate(xfg):
                if "code_sym_token" in xfg.nodes[nn]:
                    flag=True
                    break
            if flag:
                # print("INisde")
                continue
            file_path = xfg.graph["file_paths"][0]
            file_path = file_path[:74] + "source_code/" + file_path[74:]
            with open(file_path, "r", encoding="utf-8") as f:
                file_contents = f.readlines()
            # print(file_contents)
            code_lines = list()
            for n in xfg:
                code_lines.append(file_contents[n - 1])
            sym_code_lines = clean_gadget(code_lines)
            to_remove = list()
            for idx, n in enumerate(xfg):
                xfg.nodes[n]["code_sym_token"] = tokenize_code_line(sym_code_lines[idx], split_token)
                if len(xfg.nodes[n]["code_sym_token"]) == 0:
                    to_remove.append(n)
            xfg.remove_nodes_from(to_remove)
            if len(xfg.nodes) != 0:
                handle_queue_message(xfg, xfg_path, False)
            else:
                handle_queue_message(xfg, xfg_path, True)
    return source_path


def add_symlines(source_path: str, directories: str, split_token: bool):
    """

    Args:
        cweid:
        root:

    Returns:

    """

    # XFG_root_path = join(root, cweid, "XFG")
    testcaseids = []
    for i in directories:
        testcaseids.extend([source_path + "\\" + i + "\\" + j for j in os.listdir(source_path + "/" + i)])
        # print(testcaseids)

    testcase_len = len(testcaseids)
    print(testcase_len)
    process_func = functools.partial(process_parallel, split_token=split_token)
    testcaseids_done: List = [
        testcaseid
        for testcaseid in tqdm(
            map(process_func, testcaseids),
            desc=f"testcases: ",
            total=testcase_len,
        )
    ]
    # print("Count1 "+str(count1))
    # print("Count "+str(count))
    print("Remove "+str(remove))
    # ff.write("Remove Count: "+str(remove))
    # ff.close()




if __name__ == '__main__':
    __arg_parser = configure_arg_parser()
    __args = __arg_parser.parse_args()
    config = cast(DictConfig, OmegaConf.load(__args.config))
    source_path = config.data_folder_path
    root_folder_path=config.root_folder_path
    split_folder_name=config.split_folder_name
    directories = os.listdir(source_path)
    print(directories)
    # # add_symlines(source_path, directories, False)
    # xfg_unique_paths = unique_data(source_path,root_folder_path, directories)
    # with open(join(root_folder_path,'xfg_unique_paths'), 'wb') as fp:
    #     pickle.dump(xfg_unique_paths, fp)
    with open(join(root_folder_path,'xfg_unique_paths'), 'rb') as fp:
        xfg_unique_paths = pickle.load(fp)
    split_list(xfg_unique_paths, join(root_folder_path,split_folder_name))

