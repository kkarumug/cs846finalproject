import json
import networkx as nx
from collections import defaultdict,Counter
from typing import List, cast
from os.path import join
from tqdm import  tqdm
from argparse import ArgumentParser
import os
from omegaconf import OmegaConf, DictConfig
def configure_arg_parser() -> ArgumentParser:
    arg_parser = ArgumentParser()
    arg_parser.add_argument("-c",
                            "--config",
                            help="Path to YAML configuration file",
                            default="configs/dwk.yaml",
                            type=str)
    return arg_parser
def count_labels(json_path):
    set1=[]
    with open(json_path, "r") as f:
        paths = json.load(f)
    for i in tqdm(paths,total=len(paths)):
        xfg = nx.read_gpickle(i)
        set1.append(xfg.graph["label"])
    return Counter(set1)

def test_dataset_line_metrics(config,json_path):
    paths=[]
    with open(json_path, "r") as f:
        paths = json.load(f)
    # print(paths)
    seen=set()
    result=[]
    for i in paths:
        file_path="/".join(i.split("/")[-4:-2])
        # print(file_path)
        if file_path not in seen:
            if os.path.isdir(join(config.all_XFG_path,file_path)):
                poi_paths=os.listdir(join(config.all_XFG_path,file_path))
                for j in poi_paths:
                    if j!=".DS_Store":
                        xfg_paths=os.listdir(join(config.all_XFG_path,file_path,j))
                        for k in xfg_paths:
                            result.append(join(config.all_XFG_path,file_path,j,k))
                seen.add(file_path)
    print(len(result))
    with open(join(config.root_folder_path,config.split_folder_name,"test_all_xfg_paths.json"),"w") as f:
        json.dump(result, f)




    # for i in paths:
    #     print(i.split("/"))



if __name__ == '__main__':
    __arg_parser = configure_arg_parser()
    __args = __arg_parser.parse_args()
    config = cast(DictConfig, OmegaConf.load(__args.config))
    root_folder_path=config.root_folder_path
    all_XFG_path=config.all_XFG_path
    split_folder_name=config.split_folder_name
    # print("Test Label Size:",count_labels(join(root_folder_path,"all_source_code_xfgs_split","val.json")))
    test_dataset_line_metrics(config,join(root_folder_path,split_folder_name,"test.json"))
